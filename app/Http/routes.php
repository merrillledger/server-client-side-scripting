// app/Http/routes.php
<?php

// ===============================================
// STATIC PAGES ==================================
// ===============================================

// show a static view for your home page (app/resource/views/home.blade.php)
Route::resource('/', 'HomeController');

// about page (app/resources/views/skills.blade.php)
Route::resource('skills', 'SkillsController');

// about page (app/resources/views/skills.blade.php)
Route::resource('skills', 'SkillController');

// Resume page (app/resources/views/resume.blade.php)
Route::resource('resume', 'ResumeController');

// Contact page (app/resources/views/contact.blade.php)
Route::resource('contact', 'ContactController');

Route::resource('experiance', 'ExperianceController');

Route::group(['middle' => ['web']], function() {

    Route::resource('skills', 'SkillController');
    Route::resource('experiance', 'ExperianceController');

});
