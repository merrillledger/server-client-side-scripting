<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\skill;


class SkillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $skills = skill::all();

        return view('admin.skills.show', ['skills' => $skills]);
    }

    public function create()
    {
        return view('admin.skills.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'bail|required|unique:skills|min:3|max:255',
            'detail' => 'required',
            ]);

        $input = $request->all();

        skill::create($input);

        return redirect('skills');
    }    

    public function edit($id)
    {
        $skill = skill::findOrFail($id);

        return view('admin.skills.edit', compact('skill'));
    }

    public function update(Request $request, $id)
    {
        $skill = skill::findOrFail($id);

        $skill->update($request->all());

        return redirect('skills');
    }
}
