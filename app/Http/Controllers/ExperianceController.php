<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\experiance;


class SkillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $experiance = experiance::all();

        return view('resume', ['experiance' => $experiance]);
    }

    public function create()
    {
        return view('admin.experiances.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'employer' => 'bail|required|unique:experiance|min:3|max:255',
            'jobtitle' => 'required',
            'start' => 'required',
            'finish' => 'required',
            'keyskills' => 'required',
            ]);

        $input = $request->all();

        experiance::create($input);

        return redirect('experiance');
    }    
}
