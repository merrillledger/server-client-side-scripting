 <!doctype html>
  <html>
  <head>
      <meta charset="UTF-8">
      <title>Admin - Skills</title>
      <link rel="stylesheet" href="/css/app.css" />
  </head>
  <body>
  <div class="container">
      <header class="row">
          <nav class="navbar navbar-inverse navbar-fixed-top">
              <div class="container">
                  <ul class="nav navbar-nav">
                      <a class="navbar-brand" href="#">Admin</a>
                      <li class="active"><a href="/">skills</a></li>
                  </ul>
              </div>
          </nav>
      </header>
      <article class="row">
          <div class="col-md-6">
            <h1>Skills</h1>
          </div>
          <div class="col-md-6">
          <a href="skills/create" class="btn btn-lg btn-success pull-right top-buffer">Add New Skill</a>
          </div>
          <section>
              @if (isset ($skills))

                  <table class="table table-striped table-bordered">
                      <thead>
                      <tr>
                          <td>Skill</td>
                          <td>Detail</td>
                          <td>Rating 0 - 5</td>
                          <td>Update</td>
                          <td>Delete</td>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach ($skills as $skill)
                          <tr>
                              <td>{{ $skill->title }}</td>
                              <td>{{ $skill->detail }}</td>
                              <td>
                                  @for ($i = 0; $i < $skill->stars; $i++)
                                      *
                                  @endfor
                              </td>
                              <td> <a href="skills/{{ $skill->id }}/edit" class="btn btn-warning">Update</a></td>
                              <td> <a href="skills/{{ $skill->id }}/delete" class="btn btn-danger">Delete</a></td>
                          </tr>

                      @endforeach
                      </tbody>
                  </table>
              @else
                  <p> No skills added yet </p>
              @endif
          </section>
      </article>
      <footer class="row">
          @include('includes.footer')
      </footer>
  </div><!-- close container -->

  </body>
  </html>