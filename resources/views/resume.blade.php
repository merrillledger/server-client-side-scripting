@extends('layouts.master')

@section('title', 'Resume')

@section('content')
    <h1>Resume</h1>
    <p>I am a hardworking individual that has experiances in teamwork throughtout my course.</P>

    <section>
      @if (isset ($skills))

      <table class="table table-striped table-bordered">
                  <thead>
                      <tr>
                          <td>Skill</td>
                          <td>Detail</td>
                          <td>Rating 0 - 5</td>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach ($skills as $skill)
                          <tr>
                              <td>{{ $skill->title }}</td>
                              <td>{{ $skill->detail }}</td>
                              <td>@for ($i = 0; $i < $skill->stars; $i++)
                                      *
                                  @endfor
                              </td>
                          </tr>

                      @endforeach
                  </tbody>
              </table>
      @else
          <p> no skills added yet </p>
      @endif
  </section>
  <section>
      @if (isset ($qualifications))

      <table class="table table-striped table-bordered">
                  <thead>
                      <tr>
                          <td>Title</td>
                          <td>Completion Year</td>
                          <td>Institute</td>
                          <td>Grade</td>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach ($qualifications as $qualification)
                          <tr>
                              <td>{{ $qualification->title }}</td>
                              <td>{{ $qualification->completion }}</td>
                              <td>{{ $qualification->institute }}</td>
                              <td>{{ $qualification->grade }}</td>
                          </tr>

                      @endforeach
                  </tbody>
              </table>
      @else
          <p> no qualifications added yet </p>
      @endif
  </section>

@endsection