<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
<div class="container">
    <header class="row">
     @include('includes.header')
    </header>

    <article class="row">

        @yield('content')

    </article>

    <footer class="row">
        @include('includes.footer')
    </footer>
</div><!-- close container -->    
</body>
</html>