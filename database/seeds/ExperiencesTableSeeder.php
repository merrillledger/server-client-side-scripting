<?php

use Illuminate\Database\Seeder;

class ExperiencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('experiences')->insert([
            ['id' => 1, 'employer' => "RAPID ERP", 'jobtitle' => "Work Experiance", 'start' => "01/02/19", 'finish' => "07/02/19", 'keyskills' => "Communication and understanding of the IT industry"],
            ['id' => 2, 'employer' => "OUTFIT", 'jobtitle' => "Retail assistant", 'start' => "29/02/18", 'finish' => "26/08/19", 'keyskills' => "Communication, time management and teamwork"],
            ['id' => 3, 'employer' => "JD Sports", 'jobtitle' => "Retail assistant", 'start' => "04/09/20", 'finish' => "20/11/20", 'keyskills' => "Communication, time management and teamwork"],
            ['id' => 4, 'employer' => "Rivington Primary", 'jobtitle' => "Teaching assistant(WE)", 'start' => "20/03/18", 'finish' => "25/03/18", 'keyskills' => "Adujusting communication and understanding on how to look after a child"],
            ['id' => 5, 'employer' => "Greenfields Nursing Home", 'jobtitle' => "Carer(WE)", 'start' => "11/11/17", 'finish' => "16/11/17", 'keyskills' => "Empathy and communication"],

        ]);
    }
}
