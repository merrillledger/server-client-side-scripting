<?php

use Illuminate\Database\Seeder;

class QualificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('qualifications')->insert([
            ['id' => 1, 'title' => "High school graduate", 'completion' => "2017", 'institute' => "De La Salle", 'grade' => "10 GSCEs"],
            ['id' => 2, 'title' => "A-level", 'completion' => "2019", 'institute' => "Carmel College", 'grade' => "Distinction, D* AND C"],
        ]);
    }
}
